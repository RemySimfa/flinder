import 'package:flinder/devs_page.dart';
import 'package:flinder/main.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
class BioPage extends StatefulWidget {
  BioPage({this.accountUser});

  final GoogleSignInAccount accountUser;

  @override
  State<StatefulWidget> createState() {

    return new BioPageState(currentUser: accountUser);
  }

}


class BioPageState extends State<BioPage> {
  BioPageState({this.currentUser});
  GoogleSignInAccount currentUser;
  final _formKey = new GlobalKey<FormState>();



  String title = "";
  String description = "";
  String summary = "";
  String location = "Lekki Phase 1";



  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context).textTheme.caption.copyWith(
      color: Colors.white,
    );
    return new Scaffold(
      appBar: new AppBar(
        title: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: Theme.of(context).platform == TargetPlatform.iOS
              ? CrossAxisAlignment.center
              : CrossAxisAlignment.start,
          children: <Widget>[
            new Text("Ogombo Road"),
            new InkWell(
              onTap: () {},
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text("Change location", style: theme),
                  new Icon(Icons.arrow_drop_down),
                ],
              ),
            ),
          ],
        ),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.camera_alt),
            onPressed: () {},
          ),
        ],


      ),
      body: new SafeArea(
        child: _buildForm(context),
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return new Container(
      child: new Form(
        key: _formKey,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 16.0,
                ),
                child: new Column(
                  children: <Widget>[
                    TextFormField(

                      decoration: InputDecoration(
                        labelText: 'Username',
                      ),
                      validator: (value) => value.isEmpty
                          ? "Provide a title for this incident"
                          : null,

                      onSaved: (String value) {
                        setState(() {
                          title = value;
                        });
                      },

                    ),

                    TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Bio',
                          hintText: 'Tell us more about yourself'
                        ),
                        keyboardType: TextInputType.multiline,
                        maxLines: 6,
                        onSaved: (String value) {
                          description = value;
                        }
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: new SizedBox(
                width: double.infinity,
                child: new RaisedButton(
                  child: new Text("SUBMIT"),
                  onPressed: () => handleSubmit(),
                  textColor: Theme.of(context).canvasColor,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  handleSubmit() {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (context) => new Flinder()
    ));
  }

//  _handleSubmit(BuildContext context) {
//
//    if (_formKey.currentState.validate()) {
//      _formKey.currentState.save();
//      Firestore.instance.runTransaction((Transaction transaction) async  {
//        CollectionReference reference = Firestore.instance.collection("incidents");
//
//        await reference.add({
//          "email" : currentUser.email.toString(),
//          "title" : title,
//          "content" : description,
//          "summary" : summary,
//          "location" : location,
//          "author" : currentUser.displayName.toString(),
//          "reviews" : 0,
//          "views" : 0,
//          "flags" : 0,
//          "follows" :0,
//          "boosts" : 0,
//        });
//      });
//
//      Navigator.pop(context);
//    }
//
//
//  }
}