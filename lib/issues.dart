import 'dart:convert';

import 'package:flinder/bio.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:share/share.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:timeago/timeago.dart';

import 'package:google_sign_in/google_sign_in.dart';


class IssuesPage extends StatefulWidget {
  IssuesPage({this.account});
  GoogleSignInAccount account;
//  IssuesListScreen({Key key}) : super (key: key);


  @override
  IssuesPageState createState() => new IssuesPageState(currentUser: account);
}

class IssuesPageState extends State<IssuesPage> {
  IssuesPageState({this.currentUser});
  GoogleSignInAccount currentUser;

  var data;
  var issueSelection = "techcrunch";
  var snapSources;

  TimeAgo ta = new TimeAgo();
  final FlutterWebviewPlugin flutterWebviewPlugin = new FlutterWebviewPlugin();
  final TextEditingController _controller = new TextEditingController();


  /**
   * Pull Data from API
   */
  Future getData() async {
    var response = await http.get(
        Uri.encodeFull(
            'https://newsapi.org/v2/top-headlines?sources=' + issueSelection),
        headers: {
          "Accept": "application/json",
          "X-Api-Key": "ab31ce4a49814a27bbb16dd5c5c06608"
        }
    );


    var localData = JSON.decode(response.body);
    //if app is launched
    setState(() {
      data = localData;
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.getData();
  }


  /**
   * Build Screen Rows
   */

  //Build Source of Issue and time posted
  Row _buildTimePostedAndSourceRow(int index) {
    return new Row(
      children: <Widget>[

        new Padding( // for source of issue
          padding: new EdgeInsets.all(5.0),
          child: new Text(
            '',
//            data["articles"][index]["source"]["name"],
            style: new TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.grey[700],
                fontSize: 12.0
            ),
          ),
        ),

        new Padding( //for time published
          padding: new EdgeInsets.only(left: 4.0, right: 4.0),
          child: new Text(
            '5KM',
//            timeAgo(DateTime.parse(data["articles"]
//            [index]["publishedAt"])),
            style: new TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.grey[600],
              fontSize: 12.0,
            ),
          ),
        ),


        new Wrap(

          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.only(left: 35.0),
              child: new Icon(Icons.map, size: 12.0,),
            ),

            new Padding(
              padding: new EdgeInsets.only(left: 1.0, top: 0.0),
              child:  new Text("Lekki Phase 1",
                style: new TextStyle(
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[600],
                  fontSize: 12.0,
                ),
              ),
            ),

          ],
        ),


      ],


    );


  }

  Column _buildStatusItems(int indexOfPost, int values,  IconData icon, String label ) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0, left: 5.0),
              child: new Icon(icon,
                color: Colors.grey[600],
                size: 12.0,
              ),
            ),

            new Padding(padding: new EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0),

              child: new Text(values.toString(),
                style: new TextStyle(
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[600],
                  fontSize: 12.0,
                ),
              ),
            ),

            new Padding(padding: new EdgeInsets.only(top: 5.0, bottom: 5.0, right: 0.0, left: 0.0),
              child: new Text(label,
                style: new TextStyle(
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[600],
                  fontSize: 12.0,
                ),
              ),
            )

          ],
        )
      ],
    );


  }

  //Build the Image Row
  Wrap _buildIssueImageRow(int index) {
    return Wrap(
      spacing: 8.0, // gap between adjacent chips
      runSpacing: 4.0, // gap between lines
      direction: Axis.horizontal,


      children: <Widget>[
        new Padding(


          padding: new EdgeInsets.only(top: 8.0, left: 8.0),
          child: new SizedBox(

            height: 200.0,
            width: 350.0,

            child: new Image.network(


              data["articles"][index]["urlToImage"],
              fit: BoxFit.fitWidth,
              width: 500.0,
              height: 500.0,
            ),
          ),

        )
      ],
    );
  }

  Column _buildSolvedStatus(int index, IconData icon, String label) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Padding(padding: new EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0, left: 5.0),
              child: new Icon(icon,
                color: Colors.green[600],
                size: 12.0,
              ),
            ),


            new Padding(padding: new EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0),
              child: new Text(label,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Colors.grey[600],
                    fontSize: 12.0
                ),
              ),
            )

          ],
        )
      ],
    );
  }

  Column buildButtonColumny(IconData icon, String label) {
    Color color = Theme.of(context).primaryColor;

    return Column(

      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

  Column buildButtonColumn(IconData icon, String label) {
    Color color = Theme.of(context).primaryColor;

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [

        Icon(
          icon,
          color: color,
          size: 20.0,

        ),
        Container(
          margin: const EdgeInsets.only(top: 20.0),

          child: Text(
            label,
            style: TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }


  Wrap _buildTitleRow(int index) {
    return new Wrap(



      children: <Widget>[



        new Padding( //for title

          padding: new EdgeInsets.only(
              left: 8.0,
              top: 8.0,
              bottom: 1.0,
              right: 30.0
          ),

          child: new Text(
            'Remy Ohajinwa',
//            data["articles"][index]["title"],
            style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0

            ),
            textAlign: TextAlign.left,
          ),
        )
      ],
    );
  }

  Wrap _buildDescriptionSection(int index) {
    return new Wrap(

      children: <Widget>[
        new Padding( //padding for description
          padding: new EdgeInsets.only(
              left: 8.0,
              right: 34.0,
              bottom: 15.0
          ),
          child: new Text(
            data["articles"][index]["description"],
            style: new TextStyle(
              color: Colors.grey[500],
            ),
          ),
        )

      ],

    );
  }

  Row _buildDescriptionRow(int index) {
    return new Row(

      children: [
        new Expanded(


          child: new GestureDetector( //detect user click on this column

            child: _buildDescriptionSection(index),

            onTap: () {
              flutterWebviewPlugin.launch(
                  data["articles"][index]["url"],
                  fullScreen: false
              );
            },
          ),
        ),

      ],
    );
  }

  Row _buildFlagsViewsReviews(int index) {
    return new Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[


        new Expanded(child: new GestureDetector(
          child: _buildStatusItems(index, 1500, Icons.remove_red_eye, "Views"),
        )),

        new Expanded(child: new GestureDetector(
          child: _buildStatusItems(index, 50, Icons.comment, "Assists"),
        )),

      ],

    );
  }

  Row _buildFollowsSolvedBoosts(int index) {
    return new Row(
      children: <Widget>[


      ],

    );
  }


  Center _buildNoIssuesFound() {
    return new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Icon(Icons.chrome_reader_mode,
              color: Colors.grey, size: 60.0),
          new Text(
            "No Issues to Follow",
            style: new TextStyle(
                fontSize: 24.0, color: Colors.grey
            ),

          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.grey[200],

      body: new Column(children: <Widget>[

        //Search for Issues
        new Padding(
          padding: new EdgeInsets.all(0.0),
          child: new PhysicalModel(
            color: Colors.white,
            elevation: 3.0,
            child: new TextField(
              controller: _controller,
              decoration: new InputDecoration(
                  hintText: 'Search for Devs ',
                  icon: new Icon(Icons.search)
              ),
            ),
          ),
        ),

        new Expanded(
            child: data == null
                ? const Center(
                child: const CircularProgressIndicator()) //progress indicator if data could not be fetched
                : data["articles"].length != 0

                ? new ListView.builder( //build the curated list
              itemCount: data == null ? 0 : data["articles"].length,
              padding: new EdgeInsets.all(8.0),
              itemBuilder: (BuildContext context, int index) {
                return new Card( // card-like list of an item
                  elevation: 1.7,
                  child: new Padding(

                    padding: new EdgeInsets.all(10.0),


                    child: new Column(
                      children: [

                        //Row that contains time posted and the source of the issue
                        _buildTimePostedAndSourceRow(index),

                        //Row to build Issue Image
                        _buildIssueImageRow(index),

                        //build TitleRow
                        _buildTitleRow(index),

                        //Description Row and section
                        _buildDescriptionRow(index),

                        _buildFlagsViewsReviews(index),

                        _buildFollowsSolvedBoosts(index),


                      ],
                    ),

                  ),
                );
              },
            ) : _buildNoIssuesFound()
        )
      ]
      ),

      floatingActionButton: new FloatingActionButton(
        onPressed: () {

          Navigator.of(context).push(new MaterialPageRoute(

              builder: (BuildContext context) => new BioPage(accountUser : currentUser)

          ));
        },

        tooltip: 'Post an Issue',
        child: new Icon(Icons.report),
      ),
    );
  }

  printer(String email) {
    print("The current user account in IssuesList  is: "+ email);

  }
}



