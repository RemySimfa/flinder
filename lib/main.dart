import 'package:flinder/devs_page.dart';
import 'package:flinder/issues.dart';
import 'package:flutter/material.dart';
import 'pages/login/loginpage.dart';

Widget _defaultLandingPage = new LoginScreen();

void main() =>  runApp(new MaterialApp(
  home: _defaultLandingPage,
));

class Flinder extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FlinderState();
  }

}

class FlinderState  extends State with SingleTickerProviderStateMixin {
  TabController controller;


  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Flinder"),
      ),
      bottomNavigationBar: new Material(
          color: Colors.blue[600],
          child: new TabBar(controller: controller, tabs: <Tab>[
            new Tab(icon: new Icon(Icons.view_headline, size: 30.0)),
            new Tab(icon: new Icon(Icons.all_inclusive, size: 30.0),),

          ])
      ),

      body: new TabBarView(
          controller: controller,
          children: <Widget>[
            new DevsPage(),
            new IssuesPage(),


          ]),
    );
  }

}
