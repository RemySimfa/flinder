//import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
//
//void main() => runApp(new MyApp());
//
//class MyApp extends StatelessWidget {
//
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: 'Create Incident',
//      theme: new ThemeData(
//        primarySwatch: Colors.blue,
//      ),
//      home: new LoginScreen(title: 'Create Incident'),
//    );
//
//  }
//}
//
//class LoginScreen extends StatefulWidget {
//  LoginScreen({Key key, this.title}) : super(key: key);
//  final String title;
//
//  @override
//  _LoginScreenState createState() => new _LoginScreenState();
//
//}
//
//class _LoginScreenState extends State<LoginScreen> {
//  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
//  List<String> _colors = <String>['', 'red', 'green', 'blue', 'orange'];
//  String _color = '';
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text(widget.title),
//      ),
//
//      body: new SafeArea(
//          top: false,
//          bottom: false,
//          child: new Form(
//              key: _formKey,
//              autovalidate: true,
//              child: new ListView(
//                padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                children: <Widget>[
//                  new TextFormField(
//                    decoration: const InputDecoration(
//                      icon: const Icon(Icons.person),
//                      hintText: 'Enter your first and last name',
//                      labelText: 'Name',
//                    ),
//                  ),
//
//                  new TextFormField(
//                    decoration: const InputDecoration(
//                      icon: const Icon(Icons.calendar_today),
//                      hintText: "Enter your date of birth",
//                      labelText: 'DOB'
//                    ),
//                    keyboardType: TextInputType.datetime,
//                  ),
//
//                  new TextFormField(
//                    decoration: const InputDecoration(
//                      icon: const Icon(Icons.phone),
//                      hintText: 'Enter a phone number',
//                      labelText: 'Phone',
//                    ),
//                    keyboardType: TextInputType.phone,
//                    inputFormatters: [
//                      WhitelistingTextInputFormatter.digitsOnly,
//                    ],
//                  ),
//
//                  new TextFormField(
//                    decoration: const InputDecoration(
//                      icon: const Icon(Icons.email),
//                      hintText: 'Enter an email address',
//                      labelText: 'Email',
//                    ),
//                    keyboardType: TextInputType.emailAddress,
//                  ),
//
//                  new InputDecorator(
//                    decoration: const InputDecoration(
//                      icon: const Icon(Icons.color_lens),
//                      labelText: 'Color',
//                    ),
//                    isEmpty: _color == '', //check whether field is empty
//                    child: new DropdownButtonHideUnderline(
//                      child: new DropdownButton<String>(
//                          value: _color,
//                          isDense: true,
//                          onChanged: (String newValue) {
//                            setState(() {
//                              _color = newValue;
//                            });
//                          },
//                        items: _colors.map((String value) {
//                          return new DropdownMenuItem<String>(
//                            value: value,
//                              child: new Text(value)
//                          );
//
//                        }).toList(),
//
//                      ),
//                    ),
//
//                  ),
//
//                  new Container(
//                    padding: const EdgeInsets.only(left: 40.0, top: 20.0),
//                    child: new RaisedButton(
//                      child: const Text('Submit'),
//                        onPressed: null
//                    ),
//                  )
//                ],
//              )
//          )
//      ),
//    );
//  }
//
//}
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert' show json;
import 'package:flinder/bio.dart';


import 'package:google_sign_in/google_sign_in.dart';


GoogleSignIn _googleSignIn = new GoogleSignIn(
    scopes: <String> [
      'email'
    ]
);

class LoginScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new _LoginScreenState();
  }

}

class _LoginScreenState extends State<LoginScreen> {

  GoogleSignInAccount _currentUser;


  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });

      if (_currentUser != null) {
//        Navigator.of(context).push(new MaterialPageRoute(
//            builder: (context) => new LoudAm(currentUser: _currentUser,)
//        ));

      }
      _googleSignIn.signInSilently();
    });
  }

  Future<Null> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    }catch(error) {
      print(error);
    }
  }


  @override
  Widget build(BuildContext context) {


    return new Scaffold(
      body: _checkUserStatus(),
    );
  }

  _checkUserStatus() {
    if (_currentUser == null) {
      return new Builder(


          builder: (BuildContext context) => new Column(
            children: <Widget>[
              new Flexible(
                  flex: 2,
                  child: new Container(
                    color: Theme.of(context).primaryColor,
                  )
              ),

              new Flexible(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 0.0,
                      horizontal: 48.0,
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new SizedBox(
                          width: double.infinity,
                          child: new RaisedButton.icon(
                            onPressed: () => _handleSignIn(),
                            icon: new Image.asset("res/icons/google.png",
                              color: Colors.white,
                              height: 16.0,
                            ),
                            label: GestureDetector(
                                child: new Text("SIGN IN WITH GOOGLE "),
                              onTap: handleNavigation
                            ),
                            color: Colors.red[600],
                            textColor: Colors.white,
                            
                          ),
                        ),
 

                      ],
                    ),
                  )
              ),
            ],
          )
      );
    } else {

    }
  }

  handleNavigation() {
    Navigator.of(context).push(new MaterialPageRoute(
            builder: (context) => new BioPage()
        ));
  }



}